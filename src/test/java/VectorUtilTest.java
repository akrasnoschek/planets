import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class VectorUtilTest {

    @RunWith(Parameterized.class)
    public static class AddTest{









    }

    @Test
    public void getVector(){


        double direction = VectorUtil.getDirection(new Position(5, 5), new Position(10, 10));
        assertThat(direction, is(45.0));

        direction = VectorUtil.getDirection(new Position(0, 0), new Position(10, 0));
        assertThat(direction, is(90.0));

        direction = VectorUtil.getDirection(new Position(0, 0), new Position(-10, 0));
        assertThat(direction, is(270.0));

        direction = VectorUtil.getDirection(new Position(0, 0), new Position(0, -10));
        assertThat(direction, is(180.0));


       /* Vector vector = VectorUtil.getVector(new Position(0, 0), new Position(3, 0));
        assertThat(vector.getDirection(), is(0.0));
        assertThat(vector.getLength(), is(3.0));


        vector = VectorUtil.getVector(new Position(0, 0), new Position(0, 4));
        assertThat(vector.getDirection(), is(90.0));
        assertThat(vector.getLength(), is(4.0));


        vector = VectorUtil.getVector(new Position(0, 0), new Position(5, 5));
        assertThat(vector.getDirection(), is(45.0));
        assertThat(vector.getLength(), is(4.0));*/
    }

    @Test
    public void add_base(){

        assertThat(VectorUtil.add(new Vector(0,0), new Vector(2, 2)), is(new Vector(2,2)));

        assertThat(VectorUtil.add(new Vector(2,-2), new Vector(2, 2)), is(new Vector(0,0)));

//        assertThat(VectorUtil.add(new Vector(2,2), new Vector(182, 2)), is(new Vector(0,0)));
    }


    @Test
    public void convertTo(){

        assertThat(VectorUtil.convertTo(new Vector(45,10)), is(new Position(7.071067811865,7.071067811865)));
        assertThat(VectorUtil.convertTo(new Vector(135,10)), is(new Position(7.071067811865,-7.071067811865)));
        assertThat(VectorUtil.convertTo(new Vector(225,10)), is(new Position(-7.071067811865,-7.071067811865)));
        assertThat(VectorUtil.convertTo(new Vector(315,10)), is(new Position(-7.071067811865,7.071067811865)));

        assertThat(VectorUtil.convertTo(new Vector(0,10)), is(new Position(0,10)));
        assertThat(VectorUtil.convertTo(new Vector(90,10)), is(new Position(10,0)));
        assertThat(VectorUtil.convertTo(new Vector(180,10)), is(new Position(0,-10)));
        assertThat(VectorUtil.convertTo(new Vector(270,10)), is(new Position(-10,0)));

    }

    @Test
    public void convertTo_visevversa(){
        assertThat(VectorUtil.convertTo(new Position(7.071067811865,7.071067811865)), is(new Vector(45,10)));
        assertThat(VectorUtil.convertTo(new Position(7.071067811865,-7.071067811865)), is(new Vector(135,10)));
        assertThat(VectorUtil.convertTo(new Position(-7.071067811865,-7.071067811865)), is(new Vector(225,10)));
        assertThat(VectorUtil.convertTo(new Position(-7.071067811865,7.071067811865)), is(new Vector(315,10)));
        assertThat(VectorUtil.convertTo(new Position(0,10)), is(new Vector(360,10)));
        assertThat(VectorUtil.convertTo(new Position(10,0)), is(new Vector(90,10)));
        assertThat(VectorUtil.convertTo(new Position(0,-10)), is(new Vector(180,10)));
        assertThat(VectorUtil.convertTo(new Position(-10,0)), is(new Vector(270,10)));


    }

    @Test
    public void add(){


        Vector vector1 = new Vector()
                .setAngle(45)
                .setValue(100);
        Vector vector2 = new Vector()
                .setAngle(45)
                .setValue(20);

        Vector result = VectorUtil.add(vector1, vector2);

        assertThat(result.getAngle(), is(45.0));
        assertThat(result.getValue(), is(120.0));
    }

}
