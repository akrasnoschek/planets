import org.omg.PortableServer.THREAD_POLICY_ID;

import java.math.BigDecimal;

public class Maino {

    public static void main(String[] args) throws InterruptedException {


        Space space = new Space();

        for (int i = 0; i < 1; i++) {
            Planet planet;

            planet = new Planet("Sun", new Position(0,0), 1.9885e30);

            planet.setVelocity(new Vector());
            planet.setRadius(50);
            space.setPinPplanet(planet);
            space.add(planet);

            planet = new Planet("Earth", new Position(149600000000.0,0), 5.9726e24);
            Vector velocity = new Vector()
                    .setAngle(180.0)
                    .setValue(30000);
            planet.setVelocity(velocity);
            planet.setRadius(5);
            space.add(planet);

            planet = new Planet("Moon", new Position(150600000000.0,0), 5.9726e19);
            velocity = new Vector()
                    .setAngle(00.0)
                    .setValue(20000);
            planet.setVelocity(velocity);
            planet.setRadius(5);
            space.add(planet);



            planet = new Planet("March", new Position(0,149600000000.0), 1.9726e28);

            velocity = new Vector()
                    .setAngle(90.0)
                    .setValue(30000);
            planet.setVelocity(velocity);
            planet.setRadius(10);
            space.add(planet);

            planet = new Planet("Jupiter", new Position(0,249600000000.0), 1.9726e29);

            velocity = new Vector()
                    .setAngle(120.0)
                    .setValue(20000);
            planet.setVelocity(velocity);
            planet.setRadius(3);
            space.add(planet);


            Planet randomPlanet = createRandomPlante();




          /*  planet = new Planet("2", new Position(3, 20), 12);
            planet.setVelocity(new Vector(1, 10));
            space.add(planet);

            planet = new Planet("3", new Position(10, 40), 30);
            planet.setVelocity(new Vector(1, 10));
            space.add(planet);*/
        }



        while(true){
            Thread.sleep(1);
            space.processStep();
        }

    }

    private static Planet createRandomPlante() {

        Planet planet = new Planet("Earth", new Position(149600000000.0,0), 5.9726e24);
        Vector velocity = new Vector()
                .setAngle(180.0)
                .setValue(30000);
        planet.setVelocity(velocity);
        planet.setRadius(5);

        return planet;
    }
}
