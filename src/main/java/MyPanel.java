import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

public class MyPanel extends JPanel {

    private int width = 800;
    private int height = 600;

    private int minPlanetVisibleSize = 2;
    private int maxPlanetVisibleSize = 50;

    private long divider = 1000000000;


    private List<Planet> planetList;
    private Planet pinPlanet;

    public MyPanel setPinPlanet(Planet pinPlanet) {
        this.pinPlanet = pinPlanet;
        return this;
    }

    public MyPanel(List<Planet> planetList) {
        this.planetList = planetList;
        setSize(width, height);
    }


    public void computeSize(List<Planet> planetList) {


        double minimum = Double.MAX_VALUE;
        double maximum = 0;


        BigDecimal total = BigDecimal.ZERO;

        for (Planet planet : planetList) {

            double mass = planet.getMass();

            minimum = Math.min(mass, minimum);
            maximum = Math.max(mass, maximum);



            total = total.add(BigDecimal.valueOf(mass));
        }

        double rel = maximum / minimum;

        double coeff = maxPlanetVisibleSize / rel / minimum;

//        BigDecimal averageSize = total.divide(BigDecimal.valueOf(planetList.size()), MathContext.DECIMAL128);

        for (Planet planet : planetList) {


            double radi = planet.getMass() * coeff;

            if(radi < minPlanetVisibleSize){
                radi = minPlanetVisibleSize;
            }

            planet.setRadius(radi);

        }



//        System.out.println(averageSize);

    }


    @Override
    protected void paintComponent(Graphics g) {
        computeSize(planetList);
        g.clearRect(0, 0, width, height);


        int offsetX = width/2;
        int offsetY = height/2;

        int realOffetX = offsetX;
        int realOffetY = offsetY;

        if(pinPlanet != null){

            Position position = pinPlanet.getPosition();
            double mass = pinPlanet.getRadius();



            int x = (int) (position.getX() / (double) divider) + offsetX;
            int y =  offsetX - (int)(position.getY() / (double) divider);


            int diffX = offsetX - x;
            int diffY = offsetY - y;

            double v = mass / 2;
            realOffetX = offsetX + diffX -(int)v;
            realOffetY = offsetY + diffY -(int)v ;

        }

//        g.drawRect(0, 0, width, height);

        for (Planet planet : planetList) {

            Position position = planet.getPosition();
            double mass = planet.getRadius();


            int x = (int) (position.getX() / (double) divider) + realOffetX;
            int y =  realOffetY - (int)(position.getY() / (double) divider);

            double v = mass / 2;
            x -= v;
            y += v;



            if(planet.equals(pinPlanet)){

//                System.out.println("x=" + (x == 400));


            }

            g.drawOval(x, y, (int)mass, (int)mass);
        }
    }
}
