import javafx.geometry.Pos;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class VectorUtil {


    public static Vector getVector(Position p1, Position p2) {

        double x = p1.getX();
        double y = p1.getY();

        double x1 = p2.getX();
        double y1 = p2.getY();

        double diffX = x1 - x;
        double diffY = y1 - y;

        return new Vector();
    }


    public static double getDistance(Position position1, Position position2) {
        double x = position1.getX();
        double y = position1.getY();


        if(position1.getX() == 3.0){

        }

        double x1 = position2.getX();
        double y1 = position2.getY();

        double diffX = x - x1;
        double diffY = y - y1;

        double sqrt = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));

        return BigDecimal.valueOf(sqrt).setScale(11, RoundingMode.HALF_UP).doubleValue();
    }

    public static double getDirection(Position base, Position target) {
        double x = base.getX();
        double y = base.getY();

        double x1 = target.getX();
        double y1 = target.getY();

        double diffX = x1 - x;
        double diffY = y1 - y;


        double v = 0;

        if (diffX == 0.0) {
            v = diffY > 0 ? 0.0 : 180.0;
        } else if (diffY == 0.0) {
            v = diffX > 0 ? 90.0 : 270.0;
        } else {
            v = Math.toDegrees(Math.atan(diffX / diffY));
        }


        return v;
    }

    public static Vector add(Vector vector1, Vector vector2) {
        Position position1 = convertTo(vector1);
        Position position2 = convertTo(vector2);

        Position sum = add(position1, position2);

        return convertTo(sum);
    }


    public static Position convertTo(Vector vector) {
        double angle = Math.toRadians(vector.getAngle());
        double value = vector.getValue();

        double x = value * Math.sin(angle);
        double y = value * Math.cos(angle);

        try {
            y = BigDecimal.valueOf(y).setScale(12, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            e.printStackTrace();
            y=0;
        }
        try {
            x = BigDecimal.valueOf(x).setScale(12, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            e.printStackTrace();
            x=0;
        }

        if(Double.isInfinite(x) || Double.isNaN(x)){
            x=0;
        }
        return new Position(x, y);
    }

    public static Vector convertTo(Position position) {
        double x = position.getX();
        double y = position.getY();

        double distance = getDistance(new Position(0, 0), position);

        //todo not toch
        double angle = Math.atan(x / y);
        double angle1 = Math.toDegrees(angle);

        if((x >= 0 & y<0) || (x <= 0 & y<0)){
            angle1 = 180.0 + angle1;
        }

        if((x <= 0 & y>=0)){
            angle1 = 360.0 + angle1;
        }

        if(Double.isNaN(angle)){
            angle = 0;
        }

        return new Vector()
                .setValue(distance)
                .setAngle(angle1);
    }

    public static Position add(Position p1, Position p2) {
        double x = p1.getX() + p2.getX();
        double y = p1.getY() + p2.getY();
        return new Position(x, y);
    }

    public static Position sub(Position p1, Position p2) {
        return new Position(p1.getX() - p2.getX(), p1.getY() - p2.getY());
    }
}
