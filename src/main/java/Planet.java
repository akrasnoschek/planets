public class Planet {

    private String name;

    private Position position;

    private double radius;

    private Vector velocity;


    private double mass;


    public Planet(String name, Position position, double mass) {
        this.name = name;
        this.position = position;
        this.mass = mass;
    }

    public Planet(Position position, double mass) {
        this.position = position;
        this.mass = mass;
    }

    public Planet(Position position, double radius, double mass) {
        this.position = position;
        this.radius = radius;
        this.velocity = velocity;
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public Planet setName(String name) {
        this.name = name;
        return this;
    }

    public Position getPosition() {
        return position;
    }

    public Planet setPosition(Position position) {
        this.position = position;
        return this;
    }

    public double getRadius() {
        return radius;
    }

    public Planet setRadius(double radius) {
        this.radius = radius;
        return this;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public Planet setVelocity(Vector velocity) {
        this.velocity = velocity;
        return this;
    }

    public double getMass() {
        return mass;
    }

    public Planet setMass(double mass) {
        this.mass = mass;
        return this;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "name='" + name + '\'' +
                '}';
    }
}
