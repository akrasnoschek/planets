import java.util.Objects;

public class Vector {

    /**
     * A range from 0-360
     * clock-like.   12  - 0,  3 - 90,  6 - 180,
     * */
    private double angle;
    private double value;

    public Vector() {
    }

    public Vector(double angle, double value) {
        this.angle = angle;
        this.value = value;
    }

    public double getAngle() {
        return angle;
    }

    public Vector setAngle(double angle) {
        this.angle = angle;
        return this;
    }

    public double getValue() {
        return value;
    }

    public Vector setValue(double value) {
        this.value = value;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector vector = (Vector) o;
        return Double.compare(vector.angle, angle) == 0 &&
                Double.compare(vector.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(angle, value);
    }

    @Override
    public String toString() {
        return "Vector{" +
                "angle=" + angle +
                ", value=" + value +
                '}';
    }
}
