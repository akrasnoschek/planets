import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Space {

    public static final long TIME_FOR_STEP = 60 * 60 * 24 * 7 / 100;

    public static final double G = 6.6740831313131e-11;

    private List<Planet> planets = new ArrayList<Planet>();
    private Planet pinPplanet;

    public Space setPinPplanet(Planet pinPplanet) {
        this.pinPplanet = pinPplanet;

        if(jPanel != null){
            jPanel.setPinPlanet(pinPplanet);
        }
        return this;
    }

    MyPanel jPanel;

    public Space() {


        jPanel = new MyPanel(planets);

        JFrame frame = new JFrame("HelloWorldSwing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(800, 600));
        //Add the ubiquitous "Hello World" label.
        frame.getContentPane().add(jPanel);
        //Display the window.
        frame.pack();
        frame.setVisible(true);


        MouseController l = new MouseController();
        jPanel.addMouseListener(l);
        jPanel.addMouseMotionListener(l);
    }


    //computable

    Map<Planet, List<Vector>> planetToGravities = new HashMap<Planet, List<Vector>>();


    public void add(Planet planet) {
        planets.add(planet);
    }


    public void processStep() {
        planetToGravities.clear();

        int size = planets.size();

        //compute gravity
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) {
                    continue;
                }

                //gech gravity
                Planet planet1 = planets.get(i);
                Planet planet2 = planets.get(j);
                Vector gravity = getGravity(planet1, planet2);

                List<Vector> gravities = planetToGravities.get(planet1);
                if (gravities == null) {
                    gravities = new ArrayList<Vector>();
                    planetToGravities.put(planet1, gravities);
                }
                gravities.add(gravity);
            }
        }

        //change velocity vector
        for (Planet planet : planets) {
            List<Vector> vectors = planetToGravities.get(planet);
            Vector gravities = vectors.stream()
                    .reduce(VectorUtil::add)
                    .orElse(null);

            //move according ne velocit
            processNewPosiotion(planet, gravities, TIME_FOR_STEP);

        }

        jPanel.repaint();
    }

    private void processNewPosiotion(Planet planet, Vector gravity, long timeSeconds) {


//        System.out.println("planet=" + planet.getName());
        //recalc velocity
        Vector velocity = integrate(gravity, timeSeconds);
        Vector prevVelo = planet.getVelocity();
        Vector newVelo = VectorUtil.add(velocity, prevVelo);

//        System.out.println("ne velo="+ newVelo);

        planet.setVelocity(newVelo);


        Vector distance = integrate(newVelo, timeSeconds);
        Position distanceAbsolute = VectorUtil.convertTo(distance);

        Position prevPosition = planet.getPosition();

        Position newPOsiontion = VectorUtil.add(prevPosition, distanceAbsolute);
//        System.out.println("ne pos="+ newPOsiontion);


        if(checkBounds(planet, newPOsiontion)){
            planet.setPosition(newPOsiontion);
        }


//        Position endPoint = velocity.getEndPoint();
//        return new Position(position.getX() + endPoint.getX(), position.getY() + endPoint.getY());
    }

    private boolean checkBounds(Planet planet, Position newPOsiontion) {


        double radius = planet.getRadius();

        for (Planet otherPlanet : planets) {

            if(otherPlanet.equals(planet)){
                continue;
            }
            double radius2 = otherPlanet.getRadius();
            double minimalDistance = radius + radius2;
            double distance = VectorUtil.getDistance(newPOsiontion, otherPlanet.getPosition());

            if(minimalDistance > distance){
                return false;
            }
        }
        return true;
    }

    public Vector getGravity(Planet planet1, Planet planet2) {

        BigDecimal m1 = BigDecimal.valueOf(planet1.getMass());
        Position position = planet1.getPosition();

        BigDecimal m2 = BigDecimal.valueOf(planet2.getMass());
        Position position2 = planet2.getPosition();

        BigDecimal distance = BigDecimal.valueOf(VectorUtil.getDistance(position, position2));

        BigDecimal forceG = m1
                .multiply(m2)
                .multiply(BigDecimal.valueOf(G))
                .divide(distance, MathContext.DECIMAL128)
                .divide(distance, MathContext.DECIMAL128);

        BigDecimal acceleration1 = forceG.divide(m1, MathContext.DECIMAL128);


        Position sub = VectorUtil.sub(position2, position);
        Vector to = VectorUtil.convertTo(sub);

        return new Vector()
                .setAngle(to.getAngle())
                .setValue(acceleration1.doubleValue());
    }


    /**
     * acceleration -> speed
     * speed -> distance
     */
    private static Vector integrate(Vector value, long timeSec) {

        double value1 = value.getValue();

        double v = value1 * timeSec;
        return new Vector()
                .setAngle(value.getAngle())
                .setValue(v);
    }
}
