import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseController implements MouseListener, MouseMotionListener {

    private enum Mode { NONE, CIRCLE, DIRECTION, POWER }

    private Mode currentMode = Mode.NONE;





    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("clicked");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("pressed");

        if(currentMode == Mode.NONE){
            currentMode = Mode.CIRCLE;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("released");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("entered");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("exited");
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        System.out.println("dragged");
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        System.out.println("moved");
    }
}
